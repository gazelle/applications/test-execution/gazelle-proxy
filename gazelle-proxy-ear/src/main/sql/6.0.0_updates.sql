alter table pxy_connection add column initiator_hostname character varying(255);
alter table pxy_connection add column responder_hostname character varying(255);

alter table pxy_connection add column initiator_port integer;
alter table pxy_connection add column responder_port integer;

alter table pxy_connection add column proxy_channel_port integer;

alter table pxy_abstract_message add column http_rewrite boolean default false;

alter table tm_configuration add column hostname character varying(255);

CREATE TABLE tm_parameter (id BIGINT PRIMARY KEY,"key" VARCHAR(255),value VARCHAR(255));


CREATE TABLE public.tm_configuration_tm_parameter ( tm_configuration_id integer NOT NULL, additionalparameters_id bigint NOT NULL);

ALTER TABLE ONLY public.tm_configuration_tm_parameter
    ADD CONSTRAINT uk_7hmfpctmim46o7cfe3l4bl6nu UNIQUE (additionalparameters_id);

ALTER TABLE ONLY public.tm_configuration_tm_parameter
    ADD CONSTRAINT fk_7hmfpctmim46o7cfe3l4bl6nu FOREIGN KEY (additionalparameters_id) REFERENCES public.tm_parameter(id);

ALTER TABLE ONLY public.tm_configuration_tm_parameter
    ADD CONSTRAINT fk_io14nj3t5tyoxvoo45rp0no7q FOREIGN KEY (tm_configuration_id) REFERENCES public.tm_configuration(id);

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'socket_service_url','http://localhost:8081/sockets');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'channel_sync_period_seconds','900');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'socket_healthcheck_period_seconds','60');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'initial_healthcheck_delay_seconds','30');


UPDATE app_configuration SET value = '/opt/proxy/proxyPersistentChannels.json' WHERE variable = 'proxy_persistent_channels_file_path';

alter table pxy_abstract_message add column decode_success boolean default true;

