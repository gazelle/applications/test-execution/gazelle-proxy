--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: pxy_abstract_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pxy_abstract_message (
    pxy_abstract_message_type character varying(31) NOT NULL,
    id integer NOT NULL,
    date_received timestamp without time zone,
    from_ip character varying(255),
    index character varying(255),
    index_int integer,
    local_port integer,
    messagereceived bytea,
    proxy_port integer,
    proxy_side integer,
    remote_port integer,
    result_oid bytea,
    to_ip character varying(255),
    appname text,
    facility integer,
    hostname text,
    messageid text,
    payload text,
    procid text,
    severity integer,
    tag text,
    "timestamp" text,
    hl7_messagetype character varying(255),
    hl7_version character varying(255),
    dicom_filecommandset bytea,
    dicom_filetransfertsyntax text,
    dicom_affectedsopclassuid character varying(255),
    dicom_commandfield character varying(255),
    dicom_requestedsopclassuid character varying(255),
    http_headers bytea,
    http_messagetype character varying(255),
    http_rewrite boolean DEFAULT false,
    connection_id integer,
    result_xval_oid bytea,
    xval_log_oid bytea,
    decode_success boolean DEFAULT true
);


ALTER TABLE public.pxy_abstract_message OWNER TO gazelle;

--
-- Name: pxy_connection; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pxy_connection (
    id integer NOT NULL,
    uuid character varying(255),
    privacy_key character varying(255),
    secured boolean DEFAULT false NOT NULL,
    initiator_hostname character varying(255),
    initiator_port integer,
    responder_hostname character varying(255),
    responder_port integer,
    proxy_channel_port integer
);


ALTER TABLE public.pxy_connection OWNER TO gazelle;

--
-- Name: pxy_connection pxy_connection_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pxy_connection
    ADD CONSTRAINT pxy_connection_pkey PRIMARY KEY (id);
--
-- Name: pxy_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pxy_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pxy_message_id_seq OWNER TO gazelle;

--
-- Name: tm_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_configuration (
    id integer NOT NULL,
    host character varying(255),
    name text,
    port integer NOT NULL,
    proxyport integer NOT NULL,
    tmid integer NOT NULL,
    type integer,
    testinstance_id integer,
    hostname character varying(255)
);


ALTER TABLE public.tm_configuration OWNER TO gazelle;

--
-- Name: tm_configuration_tm_parameter; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_configuration_tm_parameter (
    tm_configuration_id integer NOT NULL,
    additionalparameters_id bigint NOT NULL
);


ALTER TABLE public.tm_configuration_tm_parameter OWNER TO gazelle;

--
-- Name: tm_parameter; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_parameter (
    id bigint NOT NULL,
    key character varying(255),
    value character varying(255)
);


ALTER TABLE public.tm_parameter OWNER TO gazelle;

--
-- Name: tm_step; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_step (
    id integer NOT NULL,
    date timestamp without time zone,
    stepindex integer,
    tmid integer NOT NULL,
    message_id integer,
    testinstance_id integer
);


ALTER TABLE public.tm_step OWNER TO gazelle;

--
-- Name: tm_step_receiver; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_step_receiver (
    tm_step_id integer NOT NULL,
    receivers_id integer NOT NULL
);


ALTER TABLE public.tm_step_receiver OWNER TO gazelle;

--
-- Name: tm_step_sender; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_step_sender (
    tm_step_id integer NOT NULL,
    senders_id integer NOT NULL
);


ALTER TABLE public.tm_step_sender OWNER TO gazelle;

--
-- Name: tm_testinstance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_testinstance (
    id integer NOT NULL,
    date timestamp without time zone,
    tmid integer NOT NULL
);

CREATE TABLE public.pxy_secured_channel_configuration_keystore_details
(
    id integer NOT NULL,
    loading_method character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pxy_secured_channel_configuration_keystore_details_pkey PRIMARY KEY (id)
);

CREATE TABLE public.pxy_secured_channel_configuration_truststore_details
(
    id integer NOT NULL,
    loading_method character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pxy_secured_channel_configuration_truststore_details_pkey PRIMARY KEY (id)
);

CREATE TABLE public.pxy_secured_channel_configuration
(
    id integer NOT NULL,
    mutual_authentication boolean NOT NULL,
    keystore_details_id integer,
    truststore_details_id integer,
    CONSTRAINT pxy_secured_channel_configuration_pkey PRIMARY KEY (id),
    CONSTRAINT fk_a3wqivpldncd8qoatiyntwgiy FOREIGN KEY (truststore_details_id)
        REFERENCES public.pxy_secured_channel_configuration_truststore_details (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_a985bc1x5iqeb8uicvad77gxc FOREIGN KEY (keystore_details_id)
        REFERENCES public.pxy_secured_channel_configuration_keystore_details (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.pxy_secured_channel_configuration_protocols
(
    secured_channel_configuration_id integer NOT NULL,
    protocol_types_enabled character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT fk_k7vj6qpy9wq7oixrw9jivymrs FOREIGN KEY (secured_channel_configuration_id)
        REFERENCES public.pxy_secured_channel_configuration (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.pxy_secured_channel_configuration_cipher_suites
(
    secured_channel_configuration_id integer NOT NULL,
    cipher_suite_types_enabled character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT fk_63s4kvcff6cw2u41hrr0nwtd0 FOREIGN KEY (secured_channel_configuration_id)
        REFERENCES public.pxy_secured_channel_configuration (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.pxy_connection_tls
(
    cipher_suite_between_init_proxy character varying(255) COLLATE pg_catalog."default",
    cipher_suite_between_resp_proxy character varying(255) COLLATE pg_catalog."default",
    handshake_notification_between_init_proxy character varying(255) COLLATE pg_catalog."default",
    handshake_notification_between_resp_proxy character varying(255) COLLATE pg_catalog."default",
    init_certificate_subject character varying(255) COLLATE pg_catalog."default",
    protocol_between_init_proxy character varying(255) COLLATE pg_catalog."default",
    protocol_between_resp_proxy character varying(255) COLLATE pg_catalog."default",
    proxy_certificate_subject character varying(255) COLLATE pg_catalog."default",
    renegociation_counter integer,
    resp_certificate_subject character varying(255) COLLATE pg_catalog."default",
    connection_id integer NOT NULL,
    CONSTRAINT pxy_connection_tls_pkey PRIMARY KEY (connection_id),
    CONSTRAINT fk_ig03k7eql6id2vduqhivsemg7 FOREIGN KEY (connection_id)
        REFERENCES public.pxy_connection (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE SEQUENCE public.pxy_secured_channel_configuration_keystore_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.pxy_secured_channel_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.pxy_secured_channel_configuration_truststore_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.pxy_secured_channel_configuration_truststore_details OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration_keystore_details OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration_cipher_suites OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration_protocols OWNER to gazelle;
ALTER TABLE public.pxy_connection_tls OWNER to gazelle;

ALTER TABLE public.tm_testinstance OWNER TO gazelle;


ALTER TABLE public.tm_configuration ADD COLUMN securedChannel BOOLEAN NOT NULL DEFAULT FALSE;

--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: pxy_abstract_message pxy_abstract_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pxy_abstract_message
    ADD CONSTRAINT pxy_abstract_message_pkey PRIMARY KEY (id);





--
-- Name: tm_configuration tm_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration
    ADD CONSTRAINT tm_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_parameter tm_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_parameter
    ADD CONSTRAINT tm_parameter_pkey PRIMARY KEY (id);


--
-- Name: tm_step tm_step_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step
    ADD CONSTRAINT tm_step_pkey PRIMARY KEY (id);


--
-- Name: tm_testinstance tm_testinstance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testinstance
    ADD CONSTRAINT tm_testinstance_pkey PRIMARY KEY (id);


--
-- Name: app_configuration uk_20rnkdjn5jvlvmsb5f7io4b1o; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT uk_20rnkdjn5jvlvmsb5f7io4b1o UNIQUE (variable);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: app_configuration_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX app_configuration_id_index ON public.app_configuration USING btree (id);


--
-- Name: appname_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX appname_index ON public.pxy_abstract_message USING btree (appname);


--
-- Name: connection_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX connection_id_index ON public.pxy_abstract_message USING btree (connection_id);


--
-- Name: date_received_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX date_received_index ON public.pxy_abstract_message USING btree (date_received);


--
-- Name: dicom_affectedsopclassuid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_affectedsopclassuid_index ON public.pxy_abstract_message USING btree (dicom_affectedsopclassuid);


--
-- Name: dicom_commandfield_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_commandfield_index ON public.pxy_abstract_message USING btree (dicom_commandfield);


--
-- Name: dicom_filecommandset_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_filecommandset_index ON public.pxy_abstract_message USING btree (dicom_filecommandset);


--
-- Name: dicom_filetransfertsyntax_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_filetransfertsyntax_index ON public.pxy_abstract_message USING btree (dicom_filetransfertsyntax);


--
-- Name: dicom_requestedsopclassuid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_requestedsopclassuid_index ON public.pxy_abstract_message USING btree (dicom_requestedsopclassuid);


--
-- Name: facility_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX facility_index ON public.pxy_abstract_message USING btree (facility);


--
-- Name: from_ip_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX from_ip_index ON public.pxy_abstract_message USING btree (from_ip);


--
-- Name: host_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX host_index ON public.tm_configuration USING btree (host);


--
-- Name: hostname_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX hostname_index ON public.pxy_abstract_message USING btree (hostname);


--
-- Name: id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX id_index ON public.pxy_connection USING btree (id);


--
-- Name: index_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX index_index ON public.pxy_abstract_message USING btree (index);


--
-- Name: index_int_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX index_int_index ON public.pxy_abstract_message USING btree (index_int);


--
-- Name: local_port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX local_port_index ON public.pxy_abstract_message USING btree (local_port);


--
-- Name: message_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX message_id_index ON public.tm_step USING btree (message_id);


--
-- Name: messageid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX messageid_index ON public.pxy_abstract_message USING btree (messageid);


--
-- Name: name_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX name_index ON public.tm_configuration USING btree (name);


--
-- Name: port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX port_index ON public.tm_configuration USING btree (port);


--
-- Name: proxy_port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX proxy_port_index ON public.pxy_abstract_message USING btree (proxy_port);


--
-- Name: proxy_side_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX proxy_side_index ON public.pxy_abstract_message USING btree (proxy_side);


--
-- Name: proxyport_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX proxyport_index ON public.tm_configuration USING btree (proxyport);


--
-- Name: pxy_abstract_message_type_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX pxy_abstract_message_type_index ON public.pxy_abstract_message USING btree (pxy_abstract_message_type);


--
-- Name: remote_port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX remote_port_index ON public.pxy_abstract_message USING btree (remote_port);


--
-- Name: result_oid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX result_oid_index ON public.pxy_abstract_message USING btree (result_oid);


--
-- Name: severity_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX severity_index ON public.pxy_abstract_message USING btree (severity);


--
-- Name: stepindex_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX stepindex_index ON public.tm_step USING btree (stepindex);


--
-- Name: testinstance_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX testinstance_id_index ON public.tm_configuration USING btree (testinstance_id);


--
-- Name: testinstance_id_index1; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX testinstance_id_index1 ON public.tm_step USING btree (testinstance_id);


--
-- Name: timestamp_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX timestamp_index ON public.pxy_abstract_message USING btree ("timestamp");


--
-- Name: tmid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX tmid_index ON public.tm_configuration USING btree (tmid);


--
-- Name: tmid_index1; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX tmid_index1 ON public.tm_step USING btree (tmid);


--
-- Name: to_ip_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX to_ip_index ON public.pxy_abstract_message USING btree (to_ip);


--
-- Name: type_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX type_index ON public.tm_configuration USING btree (type);


--
-- Name: uuid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX uuid_index ON public.pxy_connection USING btree (uuid);


--
-- Name: tm_step_sender fk_36lglup251ovijenl5ra7vy3k; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_sender
    ADD CONSTRAINT fk_36lglup251ovijenl5ra7vy3k FOREIGN KEY (senders_id) REFERENCES public.tm_configuration(id);


--
-- Name: pxy_abstract_message fk_39nerc9uwfwuh5ukcpmw5hob; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pxy_abstract_message
    ADD CONSTRAINT fk_39nerc9uwfwuh5ukcpmw5hob FOREIGN KEY (connection_id) REFERENCES public.pxy_connection(id);

--
-- Name: tm_configuration_tm_parameter fk_7hmfpctmim46o7cfe3l4bl6nu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_tm_parameter
    ADD CONSTRAINT fk_7hmfpctmim46o7cfe3l4bl6nu FOREIGN KEY (additionalparameters_id) REFERENCES public.tm_parameter(id);


--
-- Name: tm_step_receiver fk_7nr9rxx6dmi1kdmd90phfg8qt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_receiver
    ADD CONSTRAINT fk_7nr9rxx6dmi1kdmd90phfg8qt FOREIGN KEY (tm_step_id) REFERENCES public.tm_step(id);


--
-- Name: tm_step_receiver fk_8krrcr8hvb6ukbh64fega07y7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_receiver
    ADD CONSTRAINT fk_8krrcr8hvb6ukbh64fega07y7 FOREIGN KEY (receivers_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_step fk_d8w4qah6akmfdb99p2089lphp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step
    ADD CONSTRAINT fk_d8w4qah6akmfdb99p2089lphp FOREIGN KEY (message_id) REFERENCES public.pxy_abstract_message(id);


--
-- Name: tm_step fk_h0mxsfukadxicvpcs3gxlqnm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step
    ADD CONSTRAINT fk_h0mxsfukadxicvpcs3gxlqnm FOREIGN KEY (testinstance_id) REFERENCES public.tm_testinstance(id);


--
-- Name: tm_configuration fk_hf8i3isfu7ha2655vv85wrleb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration
    ADD CONSTRAINT fk_hf8i3isfu7ha2655vv85wrleb FOREIGN KEY (testinstance_id) REFERENCES public.tm_testinstance(id);

--
-- Name: tm_configuration_tm_parameter fk_io14nj3t5tyoxvoo45rp0no7q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_tm_parameter
    ADD CONSTRAINT fk_io14nj3t5tyoxvoo45rp0no7q FOREIGN KEY (tm_configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_step_sender fk_ljcxwmqx0atitsxd25sks1nxj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_sender
    ADD CONSTRAINT fk_ljcxwmqx0atitsxd25sks1nxj FOREIGN KEY (tm_step_id) REFERENCES public.tm_step(id);


--
-- PostgreSQL database dump complete
--

