package net.ihe.gazelle.proxy.model.tls.managers.truststore;

import net.ihe.gazelle.proxy.managers.TrustStoreManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class AlwaysTrustManager extends TrustStoreManager {

    static final Logger log = LoggerFactory.getLogger(AlwaysTrustManager.class);

    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

    @Override
    public List<X509Certificate> getTrustedCertificates() {
        return new ArrayList<>();
    }

    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        // Always trust
        log.info("UNKNOWN CLIENT CERTIFICATE: " + chain[0].getSubjectDN());
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        // Always trust
        log.info("UNKNOWN SERVER CERTIFICATE: " + chain[0].getSubjectDN());
    }

}
