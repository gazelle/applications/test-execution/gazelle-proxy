package net.ihe.gazelle.proxy.admin.model;

import net.ihe.gazelle.proxy.model.tls.keystore.CipherSuiteType;
import net.ihe.gazelle.proxy.model.tls.keystore.ProtocolType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "pxy_secured_channel_configuration", schema = "public")
@SequenceGenerator(name = "pxy_secured_channel_configuration_sequence", sequenceName = "pxy_secured_channel_configuration_id_seq", allocationSize = 1)
public class SecuredChannelConfiguration implements Serializable {

    private static final long serialVersionUID = -5675281833538764596L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pxy_secured_channel_configuration_sequence")
    private Integer id;

    @ElementCollection(targetClass = ProtocolType.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = "pxy_secured_channel_configuration_protocols", joinColumns = @JoinColumn(name = "secured_channel_configuration_id"))
    @Column(name = "protocol_types_enabled", nullable = false)
    @Enumerated(EnumType.STRING)
    private List<ProtocolType> protocolTypesEnabled;

    @ElementCollection(targetClass = CipherSuiteType.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = "pxy_secured_channel_configuration_cipher_suites", joinColumns = @JoinColumn(name = "secured_channel_configuration_id"))
    @Column(name = "cipher_suite_types_enabled", nullable = false)
    @Enumerated(EnumType.STRING)
    private List<CipherSuiteType> cipherSuiteTypesEnabled;

    @OneToOne
    @JoinColumn(name = "keystore_details_id", referencedColumnName = "id")
    private KeyStoreDetails keyStoreDetails;

    @OneToOne
    @JoinColumn(name = "truststore_details_id", referencedColumnName = "id")
    private TrustStoreDetails trustStoreDetails;

    @Column(name = "mutual_authentication", nullable = false)
    private boolean mutualAuthentication;

    public boolean isMutualAuthentication() {
        return mutualAuthentication;
    }

    public void setMutualAuthentication(boolean mutualAuthentication) {
        this.mutualAuthentication = mutualAuthentication;
    }

    public TrustStoreDetails getTrustStoreDetails() {
        return trustStoreDetails;
    }

    public void setTrustStoreDetails(TrustStoreDetails trustStoreDetails) {
        this.trustStoreDetails = trustStoreDetails;
    }

    public KeyStoreDetails getKeyStoreDetails() {
        return keyStoreDetails;
    }

    public void setKeyStoreDetails(KeyStoreDetails keyStoreDetails) {
        this.keyStoreDetails = keyStoreDetails;
    }

    public List<ProtocolType> getProtocolTypesEnabled() {
        return protocolTypesEnabled;
    }

    public void setProtocolTypesEnabled(List<ProtocolType> protocolTypesEnabled) {
        this.protocolTypesEnabled = protocolTypesEnabled;
    }

    public List<CipherSuiteType> getCipherSuiteTypesEnabled() {
        return cipherSuiteTypesEnabled;
    }

    public void setCipherSuiteTypesEnabled(List<CipherSuiteType> cipherSuiteTypesEnabled) {
        this.cipherSuiteTypesEnabled = cipherSuiteTypesEnabled;
    }
}
