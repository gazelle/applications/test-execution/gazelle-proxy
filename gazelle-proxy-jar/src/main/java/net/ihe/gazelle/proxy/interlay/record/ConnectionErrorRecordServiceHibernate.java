package net.ihe.gazelle.proxy.interlay.record;

import net.ihe.gazelle.proxy.application.record.RecordException;
import net.ihe.gazelle.proxy.interlay.serial.ConnectionError;

public class ConnectionErrorRecordServiceHibernate extends RecordServiceHibernate {

    @Override
    public String record(Object content) {
        if(!(content instanceof ConnectionError)){
            throw new RecordException("Object "+content.getClass()+" is not an iterator");
        }
        ConnectionError connectionError = (ConnectionError) content;
        String connectionId = super.record(connectionError.getConnection());
        connectionError.getConnection().setId(Integer.parseInt(connectionId));
        super.record(connectionError.getMessage());
        return connectionId;
    }
}
