package net.ihe.gazelle.proxy.model.tls.keystore;

import java.util.Arrays;
import java.util.List;

/**
 * This enum represents all supported cipher suites by the JDK 17.
 */
public enum CipherSuiteType {

    SSL_NULL_WITH_NULL_NULL(ProtocolType.TLSv1, ProtocolType.TLSv11, ProtocolType.TLSv12),
    TLS_RSA_WITH_NULL_MD5(ProtocolType.TLSv1, ProtocolType.TLSv11, ProtocolType.TLSv12),
    TLS_RSA_WITH_NULL_SHA(ProtocolType.TLSv1, ProtocolType.TLSv11, ProtocolType.TLSv12),

    // TLS 1.3 cipher suites. (RFC 8446)
    TLS_AES_256_GCM_SHA384(ProtocolType.TLSv13),
    TLS_AES_128_GCM_SHA256(ProtocolType.TLSv13),
    TLS_CHACHA20_POLY1305_SHA256(ProtocolType.TLSv13),

    // Added with recommendation for TLS and DTLS (BCP195) et AEAD (RFC 5116),
    TLS_DHE_RSA_WITH_AES_128_GCM_SHA256(ProtocolType.TLSv12),
    TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256(ProtocolType.TLSv12),
    TLS_DHE_RSA_WITH_AES_256_GCM_SHA384(ProtocolType.TLSv12),
    TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384(ProtocolType.TLSv12),

    // TLS 1.2 cipher suites. (RFC 5246)

    TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384(ProtocolType.TLSv12),
    TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256(ProtocolType.TLSv12),

    TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256(ProtocolType.TLSv12),

    TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256(ProtocolType.TLSv12),

    TLS_DHE_RSA_WITH_CHACHA20_POLY1305_SHA256(ProtocolType.TLSv12),
    TLS_DHE_DSS_WITH_AES_256_GCM_SHA384(ProtocolType.TLSv12),

    TLS_DHE_DSS_WITH_AES_128_GCM_SHA256(ProtocolType.TLSv12),

    TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384(ProtocolType.TLSv12),
    TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384(ProtocolType.TLSv12),

    TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256(ProtocolType.TLSv12),
    TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256(ProtocolType.TLSv12),

    TLS_DHE_RSA_WITH_AES_256_CBC_SHA256(ProtocolType.TLSv12),
    TLS_DHE_DSS_WITH_AES_256_CBC_SHA256(ProtocolType.TLSv12),

    TLS_DHE_RSA_WITH_AES_128_CBC_SHA256(ProtocolType.TLSv12),
    TLS_DHE_DSS_WITH_AES_128_CBC_SHA256(ProtocolType.TLSv12),


    TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384(ProtocolType.TLSv12),
    TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384(ProtocolType.TLSv12),

    TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256(ProtocolType.TLSv12),
    TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256(ProtocolType.TLSv12),

    TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384(ProtocolType.TLSv12),
    TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384(ProtocolType.TLSv12),

    TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256(ProtocolType.TLSv12),
    TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256(ProtocolType.TLSv12),

    TLS_RSA_WITH_AES_256_GCM_SHA384(ProtocolType.TLSv12),

    TLS_RSA_WITH_AES_128_GCM_SHA256(ProtocolType.TLSv12),

    TLS_RSA_WITH_AES_256_CBC_SHA256(ProtocolType.TLSv12),

    TLS_RSA_WITH_AES_128_CBC_SHA256(ProtocolType.TLSv12),

    // Legacy cipher suites.
    TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_DHE_RSA_WITH_AES_256_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_DHE_DSS_WITH_AES_256_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_DHE_RSA_WITH_AES_128_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_DHE_DSS_WITH_AES_128_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_ECDH_RSA_WITH_AES_256_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_ECDH_RSA_WITH_AES_128_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),



    TLS_RSA_WITH_AES_256_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_RSA_WITH_AES_128_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),
    SSL_RSA_WITH_3DES_EDE_CBC_SHA(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    TLS_EMPTY_RENEGOTIATION_INFO_SCSV(ProtocolType.TLSv12, ProtocolType.TLSv11, ProtocolType.TLSv1),

    ;

    private final ProtocolType[] protocolTypes;

    CipherSuiteType(ProtocolType... protocolTypes) {
        this.protocolTypes = protocolTypes;
    }

    public static List<CipherSuiteType> getCipherSuiteRecommendationForTLSv12(){
        return Arrays.asList(TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, TLS_DHE_RSA_WITH_AES_256_GCM_SHA384, TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384);
    }

    @Override
    public String toString() {
        return name();
    }

    public ProtocolType[] getProtocolTypes() {
        return protocolTypes;
    }

    /**
     * Returns an array containing the constants of this CipherSuite enum type, in the alphabetical order.
     *
     * @return an array containing the constants of this CipherSuite enum type, in the alphabetical order.
     */
    public static CipherSuiteType[] sortedValues() {
        CipherSuiteType[] cipherSuiteTypes = values();
        Arrays.sort(cipherSuiteTypes, new CipherSuiteComparator());
        return cipherSuiteTypes;
    }
}
