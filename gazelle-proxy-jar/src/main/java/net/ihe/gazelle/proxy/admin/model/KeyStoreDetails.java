package net.ihe.gazelle.proxy.admin.model;

import net.ihe.gazelle.proxy.model.tls.keystore.KeyStoreLoadingMethod;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "pxy_secured_channel_configuration_keystore_details", schema = "public")
@SequenceGenerator(name = "pxy_secured_channel_configuration_keystore_details_sequence", sequenceName = "pxy_secured_channel_configuration_keystore_details_id_seq", allocationSize = 1)
public class KeyStoreDetails implements Serializable {

    private static final long serialVersionUID = 7346428531980052286L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pxy_secured_channel_configuration_keystore_details_sequence")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "loading_method", nullable = false)
    private KeyStoreLoadingMethod loadingMethod;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public KeyStoreLoadingMethod getLoadingMethod() {
        return loadingMethod;
    }

    public void setLoadingMethod(KeyStoreLoadingMethod loadingMethod) {
        this.loadingMethod = loadingMethod;
    }

}
