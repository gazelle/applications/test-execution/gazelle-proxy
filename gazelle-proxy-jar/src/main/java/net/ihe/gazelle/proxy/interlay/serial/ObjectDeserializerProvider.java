package net.ihe.gazelle.proxy.interlay.serial;

import net.ihe.gazelle.proxy.application.factory.ObjectDeserializerFactory;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.model.ws.Item;

import java.util.ServiceLoader;

public class ObjectDeserializerProvider {

    /**
     * Create a content deserializer for the given item
     * @param item the item to create the deserializer for
     * @return the content deserializer
     * @throws IllegalArgumentException if the type is unknown
     * @throws ClassCastException if the type is not the expected one
     */
    public <T> ObjectDeserializer<T> createObjectDeserializer(Item item) {

        ServiceLoader<ObjectDeserializerFactory> loader = ServiceLoader.load(ObjectDeserializerFactory.class);
        for (ObjectDeserializerFactory factory : loader) {
            if (factory.enabled(item)) {
                return factory.createObjectDeserializer();
            }
        }
        throw new IllegalArgumentException("Unknown Object Deserializer type " + item.getType());
    }
}
