package net.ihe.gazelle.proxy.interlay.serial.message;

import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.HTTPMessage;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpMessageDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpRequestDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpResponseDTO;

import java.util.Map;

public class HttpMessageTransformer extends  AbstractMessageTransformer<HttpMessageDTO, HTTPMessage> {

    @Override
    public HTTPMessage transform(HttpMessageDTO message, Connection connection) {
        HTTPMessage httpMessage = super.initMessage(message, connection);
        if(message.getUnexpectedErrors() != null){
            httpMessage.setHeaders("No headers provided".getBytes());
            //content is already set in initMessage if there is an error
        }
        else{
            httpMessage.setHeaders(parseHeaders(message));
            //Override content with real http payload instead of raw message
            httpMessage.setMessageReceived(message.getBody());
        }
        httpMessage.setMessageType(getMessageType(message.getHeaders().get("Content-Type")));
        if(message.getAdditionalParameters() != null && message.getAdditionalParameters().containsKey(HttpMessageDTO.HTTP_REWRITE_KEY)){
            httpMessage.setHttpRewrite(Boolean.parseBoolean(message.getAdditionalParameters().get(HttpMessageDTO.HTTP_REWRITE_KEY)));
        }
        return httpMessage;
    }

    @Override
    protected HTTPMessage createMessage() {
        return new HTTPMessage();
    }

    private byte[] parseHeaders(HttpMessageDTO message) {
        StringBuilder headersBuilder = new StringBuilder();
        if (message instanceof HttpResponseDTO){
            HttpResponseDTO httpResponseDTO = (HttpResponseDTO) message;
            headersBuilder.append(httpResponseDTO.getVersion())
                    .append(" ")
                    .append(httpResponseDTO.getStatus())
                    .append("\r\n");
        }
        else{
            // suppose it's a request
            headersBuilder = parseHttpRequestHeaders((HttpRequestDTO) message);
        }
        for (Map.Entry<String, String> entry : message.getHeaders().entrySet()) {
            headersBuilder.append(entry.getKey()).append(": ").append(entry.getValue()).append("\r\n");
        }
        return headersBuilder.toString().getBytes();
    }

    private StringBuilder parseHttpRequestHeaders(HttpRequestDTO message){
        StringBuilder headersBuilder = new StringBuilder();
        headersBuilder.append(message.getMethod())
                .append(" ")
                .append(message.getUri())
                .append(" ")
                .append(message.getVersion())
                .append("\r\n");
        return headersBuilder;
    }

    private String getMessageType(String contentType){
        if(contentType == null) return "text/html";
        return contentType.split(";")[0];
    }
}