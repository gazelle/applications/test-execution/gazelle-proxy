package net.ihe.gazelle.proxy.interlay.ws;

import net.ihe.gazelle.proxy.application.factory.RecordServiceFactory;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.application.record.RecordException;
import net.ihe.gazelle.proxy.application.record.RecordService;
import net.ihe.gazelle.proxy.application.record.SerializationException;
import net.ihe.gazelle.proxy.interlay.factory.RecordServiceFactoryImpl;
import net.ihe.gazelle.proxy.interlay.serial.ObjectDeserializerProvider;
import net.ihe.gazelle.proxy.model.ws.Item;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Name("recordServiceWS")
@Stateless
@AutoCreate
@Path("/items")
@Scope(ScopeType.APPLICATION)
public class RecordServiceWS {

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(RecordServiceWS.class);
    public static final String ERROR_WHILE_DESERIALIZING_ITEM = "Error while deserializing item";


    private final ObjectDeserializerProvider objectDeserializerProvider = new ObjectDeserializerProvider();

    private final RecordServiceFactory recordServiceFactory = new RecordServiceFactoryImpl();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response recordItems(String body) {
        logger.info("Received item: {}", body);
        try {
            Item item = new ItemDeserializer().deserialize(body);

            ObjectDeserializer<Object> objectDeserializer = objectDeserializerProvider.createObjectDeserializer(item);
            Object content = objectDeserializer.deserialize(item.getContent(), item);

            RecordService recordService = recordServiceFactory.createRecordService(item);
            String recordId = recordService.record(content);

            logger.info("Item saved with id: {} and type: {}", recordId, item.getType());

            return Response.status(Response.Status.CREATED).header("Location", "/items/" + recordId).build();

        }
        catch (SerializationException e) {
            logger.error(ERROR_WHILE_DESERIALIZING_ITEM, e);
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        catch (IllegalStateException e){
            logger.error(ERROR_WHILE_DESERIALIZING_ITEM, e);
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
        catch (RecordException e){
            logger.error(ERROR_WHILE_DESERIALIZING_ITEM, e);
            return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).entity(e.getMessage()).build();
        }
        catch (Exception e) {
            logger.error(ERROR_WHILE_DESERIALIZING_ITEM, e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}