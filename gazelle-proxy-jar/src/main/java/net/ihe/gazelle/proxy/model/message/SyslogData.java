package net.ihe.gazelle.proxy.model.message;

public interface SyslogData {

    int getFacility();

    String getHostName();

    int getSeverity();

    String getTimestamp();

    String getTag();

    String getAppName();

    String getMessageId();

    String getProcId();

    String getPayload();

    String getRawMessage();


}
