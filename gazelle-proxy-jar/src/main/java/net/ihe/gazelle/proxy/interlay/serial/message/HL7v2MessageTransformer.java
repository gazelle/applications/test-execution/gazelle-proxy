package net.ihe.gazelle.proxy.interlay.serial.message;

import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.HL7v2Message;
import net.ihe.gazelle.proxy.model.ws.messages.HL7v2MessageDTO;

public class HL7v2MessageTransformer extends AbstractMessageTransformer<HL7v2MessageDTO, HL7v2Message> {

    @Override
    public HL7v2Message transform(HL7v2MessageDTO message, Connection connection) {
        HL7v2Message hl7v2Message = super.initMessage(message, connection);
        hl7v2Message.setHl7MessageType(message.getHl7MessageType());
        hl7v2Message.setHl7Version(message.getHl7version());
        return hl7v2Message;
    }

    @Override
    protected HL7v2Message createMessage() {
        return new HL7v2Message();
    }

}
