package net.ihe.gazelle.proxy.model.ws;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import net.ihe.gazelle.proxy.model.ws.messages.DicomMessageDTO;
import net.ihe.gazelle.proxy.model.ws.messages.HL7v2MessageDTO;
import net.ihe.gazelle.proxy.model.ws.messages.SyslogMessageDTO;
import net.ihe.gazelle.proxy.model.ws.messages.TcpMessageDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpMessageDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpRequestDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpResponseDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = HL7v2MessageDTO.class, name = HL7v2MessageDTO.TYPE),
        @JsonSubTypes.Type(value = TcpMessageDTO.class, name = TcpMessageDTO.TYPE),
        @JsonSubTypes.Type(value = SyslogMessageDTO.class, name = SyslogMessageDTO.TYPE),
        @JsonSubTypes.Type(value = HttpRequestDTO.class, name = HttpRequestDTO.TYPE),
        @JsonSubTypes.Type(value = HttpResponseDTO.class, name = HttpResponseDTO.TYPE),
        @JsonSubTypes.Type(value = HttpMessageDTO.class, name = HttpMessageDTO.TYPE),
        @JsonSubTypes.Type(value = DicomMessageDTO.class, name = DicomMessageDTO.TYPE)
})
public class Message implements Serializable {

    public static final String TYPE = "MESSAGE";

    private Date captureDate;
    private Actor receiver;
    private Actor sender;
    private byte[] content;

    private UnexpectedErrors unexpectedErrors;

    private Map<String, String> additionalParameterDTOs;

    public byte[] getContent() {
        return content;
    }

    public Message setContent(byte[] content) {
        this.content = content;
        return this;
    }

    public Actor getReceiver() {
        return receiver;
    }

    public Actor getSender() {
        return sender;
    }


    public Date getCaptureDate() {
        return captureDate;
    }



    public Message setReceiver(Actor receiver) {
        this.receiver = receiver;
        return this;
    }

    public Message setSender(Actor sender) {
        this.sender = sender;
        return this;
    }


    public Message setCaptureDate(Date captureDate) {
        this.captureDate = captureDate;
        return this;
    }

    public UnexpectedErrors getUnexpectedErrors() {
        return unexpectedErrors;
    }

    public Message setUnexpectedErrors(UnexpectedErrors unexpectedErrors) {
        this.unexpectedErrors = unexpectedErrors;
        return this;
    }

    public Map<String, String> getAdditionalParameters() {
        return additionalParameterDTOs;
    }

    public Message setAdditionalParameters(Map<String, String> additionalParameterDTOs) {
        this.additionalParameterDTOs = additionalParameterDTOs;
        return this;
    }
}


