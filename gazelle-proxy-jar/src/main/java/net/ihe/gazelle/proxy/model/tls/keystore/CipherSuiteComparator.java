package net.ihe.gazelle.proxy.model.tls.keystore;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Used to sort alphabetically {@link CipherSuiteType} enumeration values.
 */
public class CipherSuiteComparator implements Comparator<CipherSuiteType>, Serializable {

    public CipherSuiteComparator() {
    }

    @Override
    public int compare(CipherSuiteType cipher1, CipherSuiteType cipher2) {
        return cipher1.name().compareTo(cipher2.name());
    }

}
