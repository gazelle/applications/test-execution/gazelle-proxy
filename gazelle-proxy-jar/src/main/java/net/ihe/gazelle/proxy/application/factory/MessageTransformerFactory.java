package net.ihe.gazelle.proxy.application.factory;

import net.ihe.gazelle.proxy.application.record.MessageTransformer;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.ws.Message;

import java.util.List;

public interface MessageTransformerFactory {

         MessageTransformer<? extends Message, ? extends AbstractMessage> createMessageTransformer();

        List<Class<? extends Message>> getTypes();
}
