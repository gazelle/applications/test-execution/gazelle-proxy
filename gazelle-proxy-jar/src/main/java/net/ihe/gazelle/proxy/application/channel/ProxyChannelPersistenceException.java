package net.ihe.gazelle.proxy.application.channel;

public class ProxyChannelPersistenceException extends RuntimeException{

    public ProxyChannelPersistenceException(String message) {
        super(message);
    }

    public ProxyChannelPersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProxyChannelPersistenceException(Throwable cause) {
        super(cause);
    }
}
