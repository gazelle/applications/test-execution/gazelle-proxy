package net.ihe.gazelle.proxy.interlay.factory.message;

import net.ihe.gazelle.proxy.admin.gui.AbstractApplicationConfigurationManager;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.application.factory.DicomStorageService;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class FileDicomStorageService implements DicomStorageService {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(
            "yyyyMMdd-HHmmss.SSS");

    private static final Random RANDOM = new Random();

    private static final String DICOM_STORAGE = ApplicationConfiguration
            .getValueOfVariable("storage_dicom");


    @Override
    public String saveDicomDataset(byte[] dicomDataset) {
        try {
            File dicomFile = getNewFile(DICOM_STORAGE);
            org.apache.commons.io.FileUtils.writeByteArrayToFile(dicomFile, dicomDataset);
            return dicomFile.getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalStateException("Could not save dicom dataset", e);
        }
    }

    private File getNewFile(String rootPath) throws IOException {
        int i = RANDOM.nextInt(Integer.MAX_VALUE);
        File newFile;
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String year = Integer.toString(cal.get(Calendar.YEAR));
        String month = Integer.toString(cal.get(Calendar.MONTH) + 1);
        String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));

        String fileName = "DataSet_" + SIMPLE_DATE_FORMAT.format(date);

        newFile = new File(rootPath);
        newFile = new File(newFile, year);
        newFile = new File(newFile, month);
        newFile = new File(newFile, day);
        fileName += "_" + i;
        newFile = new File(newFile, fileName);
        newFile.getParentFile().mkdirs();
        if (!newFile.getParentFile().exists()) {
            throw new IOException("Could not create directory " + newFile.getParentFile().getAbsolutePath());
        }
        return newFile;
    }
}

