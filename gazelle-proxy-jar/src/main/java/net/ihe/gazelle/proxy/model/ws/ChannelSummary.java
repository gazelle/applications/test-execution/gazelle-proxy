package net.ihe.gazelle.proxy.model.ws;

import net.ihe.gazelle.proxy.model.ws.tls.Certificate;

import java.io.Serializable;
import java.util.List;

public class ChannelSummary implements Serializable {

    private int proxyPort;

    private String type;
    private TLSParameters initiatorTLSParameters;

    private TLSParameters responderTLSParameters;

    private List<Certificate> proxyCertificateChain;



    private Actor responder;

    public int getProxyPort() {
        return proxyPort;
    }



    public ChannelSummary setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
        return this;
    }

    public TLSParameters getInitiatorTLSParameters() {
        return initiatorTLSParameters;
    }

    public ChannelSummary setInitiatorTLSParameters(TLSParameters initiatorTLSParameters) {
        this.initiatorTLSParameters = initiatorTLSParameters;
        return this;
    }

    public TLSParameters getResponderTLSParameters() {
        return responderTLSParameters;
    }

    public ChannelSummary setResponderTLSParameters(TLSParameters responderTLSParameters) {
        this.responderTLSParameters = responderTLSParameters;
        return this;
    }

    public String getType() {
        return type;
    }

    public ChannelSummary setType(String type) {
        this.type = type;
        return this;
    }

    public Actor getResponder() {
        return responder;
    }

    public ChannelSummary setResponder(Actor responder) {
        this.responder = responder;
        return this;
    }

    public List<Certificate> getProxyCertificateChain() {
        return proxyCertificateChain;
    }

    public ChannelSummary setProxyCertificateChain(List<Certificate> proxyCertificateChain) {
        this.proxyCertificateChain = proxyCertificateChain;
        return this;
    }

    @Override
    public String toString() {
        return "ChannelSummary{" +
                "proxyPort=" + proxyPort +
                ", type='" + type + '\'' +
                ", initiatorTLSParameters=" + initiatorTLSParameters +
                ", responderTLSParameters=" + responderTLSParameters +
                ", responder=" + responder +
                '}';
    }
}
