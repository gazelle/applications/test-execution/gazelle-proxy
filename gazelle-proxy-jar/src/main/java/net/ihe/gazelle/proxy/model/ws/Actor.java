package net.ihe.gazelle.proxy.model.ws;

import java.io.Serializable;

public class Actor implements Serializable {
    private String ip;
    private String hostname;
    private int port;

    public String getIp() {
        return ip;
    }

    public String getHostname() {
        return hostname;
    }
    public int getPort() {
        return port;
    }

    public Actor setIp(String ip) {
        this.ip = ip;
        return this;
    }
    public Actor setHostname(String hostname) {
        this.hostname = hostname;
        return this;
    }
    public Actor setPort(int port) {
        this.port = port;
        return this;
    }

    @Override
    public String toString() {
        return "Actor [" +
                "ip='" + ip + '\'' +
                ", hostname='" + hostname + '\'' +
                ", port=" + port +
                ']';
    }
}

