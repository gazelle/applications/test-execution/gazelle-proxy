package net.ihe.gazelle.proxy.model.tls.keystore;

public enum ProtocolType {

    TLSv1("TLSv1", true),
    TLSv11("TLSv1.1", true),
    TLSv12("TLSv1.2", false),
    TLSv13("TLSv1.3", false);


    private String name;
    // To maintain
    boolean deprecated;

    ProtocolType(String name, boolean deprecated) {
        this.name = name;
        this.deprecated = deprecated;
    }

    public String getName() {
        return name;
    }

    public boolean isDeprecated() {
        return deprecated;
    }

    public String toString() {
        return getName();
    }

    public static ProtocolType getProtocolTypeFromName(String name) {
        if("NONE".equals(name)) {
            return null;
        }
        if (name != null && !name.isEmpty()) {
            for (ProtocolType value : ProtocolType.values()) {
                if (value.getName().equals(name)) {
                    return value;
                }
            }
            throw new IllegalArgumentException("No enum constant " + ProtocolType.class.getCanonicalName() + "." + name);
        } else {
            throw new IllegalArgumentException("Name cannot be null or empty to find " + ProtocolType.class.getCanonicalName() + " enum value");
        }
    }
}
