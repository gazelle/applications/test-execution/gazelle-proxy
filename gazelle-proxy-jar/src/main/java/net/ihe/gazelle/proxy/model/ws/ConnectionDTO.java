package net.ihe.gazelle.proxy.model.ws;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class ConnectionDTO implements Serializable {

    public final static String TYPE = "CONNECTION";

    public static final String TLS_ERROR_TYPE = "TLS_ERROR";

    public static final String RESPONDER_ERROR_TYPE = "RESPONDER_ERROR";

    public static final String CONNECTION_ERROR_KEY = "CONNECTION_ERROR";


    private ChannelSummary channelSummary;
    private Actor initiator;

    private UnexpectedErrors unexpectedErrors;

    public ChannelSummary getChannelSummary() {
        return channelSummary;
    }


    public Actor getInitiator() {
        return initiator;
    }

    public ConnectionDTO setChannelSummary(ChannelSummary channelSummary) {
        this.channelSummary = channelSummary;
        return this;
    }


    public ConnectionDTO setInitiator(Actor initiator) {
        this.initiator = initiator;
        return this;
    }

    @JsonIgnore
    public Actor getResponder() {
        return channelSummary.getResponder();
    }

    public UnexpectedErrors getUnexpectedErrors() {
        return unexpectedErrors;
    }

    public ConnectionDTO setUnexpectedErrors(UnexpectedErrors unexpectedErrors) {
        this.unexpectedErrors = unexpectedErrors;
        return this;
    }
}


