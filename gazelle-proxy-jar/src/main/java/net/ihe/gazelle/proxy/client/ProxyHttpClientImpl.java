package net.ihe.gazelle.proxy.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import net.ihe.gazelle.proxy.model.ws.channel.SocketRequest;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.jboss.seam.annotations.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;


public class ProxyHttpClientImpl implements ProxyHttpClient{

    private static final Logger logger = LoggerFactory.getLogger(ProxyHttpClientImpl.class);

    private final String socketServiceUrl;
    private final ObjectMapper objectMapper = new ObjectMapper()
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false)
            ;



    public ProxyHttpClientImpl(){
        socketServiceUrl = ApplicationConfiguration.getValueOfVariable("socket_service_url");
        if(socketServiceUrl == null){
            throw new ProxyClientException("socket_service_url is not defined");
        }
    }

    @Override
    public List<ProxyChannel> startChannels(SocketRequest socketRequest) throws ProxyClientException {
        if(socketServiceUrl == null){
            throw new ProxyClientException("socket_service_url is not defined");
        }
        if(socketRequest == null || socketRequest.getProxyChannels().isEmpty()){
            throw new ProxyClientException("socketRequest must not be null and must contain at least one proxy channel");
        }
        HttpClient httpClient = new HttpClient();
        PostMethod postMethod = new PostMethod(socketServiceUrl);
        postMethod.setRequestHeader("Content-Type", "application/json");
        try {
            String socketRequestString = objectMapper.writeValueAsString(socketRequest);
            postMethod.setRequestEntity(new StringRequestEntity(
                    socketRequestString,
                    "application/json",
                    "UTF-8"
            ));
            int status = httpClient.executeMethod(postMethod);
            InputStream is = postMethod.getResponseBodyAsStream();
            byte[] bytes = new byte[is.available()];
            is.read(bytes);
            String response = new String(bytes);
            if(status != 200){
                logger.error("Error while starting socket channel with status: {}, body: {}", status, response);
                SocketServiceStatus.INSTANCE.setConnected(false);
                throw new ProxyClientException("Error while starting socket channel with status:"+status);
            }
            SocketServiceStatus.INSTANCE.setConnected(true);
            return objectMapper.readValue(response, new TypeReference<List<ProxyChannel>>() {});
        } catch (ProxyClientException e) {
            SocketServiceStatus.INSTANCE.setConnected(false);
            throw e;
        }catch (Exception e) {
            SocketServiceStatus.INSTANCE.setConnected(false);
            throw new ProxyClientException(e);
        }

    }

    @Override
    public void stopChannel(int port) throws ProxyClientException {
        if(port <= 0){
            throw new ProxyClientException("port must be greater than 0");
        }
        try {
            HttpClient httpClient = new HttpClient();
            DeleteMethod deleteMethod = new DeleteMethod(socketServiceUrl+"/"+port);
            httpClient.executeMethod(deleteMethod);
            SocketServiceStatus.INSTANCE.setConnected(true);
            if(deleteMethod.getStatusCode() != 204){
                logger.error("Error while stopping socket channel {} with status: {}", port, deleteMethod.getStatusCode());
                throw new ChannelStopException("Error while stopping socket channel "+port);
            }
        } catch (IOException e) {
            SocketServiceStatus.INSTANCE.setConnected(false);
            throw new ProxyClientException(e);
        }

    }


    @Override
    public List<ProxyChannel> getAllStartedChannels() throws ProxyClientException {
        if(socketServiceUrl == null){
            throw new ProxyClientException("socket_service_url is not defined");
        }
        try {
            HttpClient httpClient = new HttpClient();
            GetMethod getMethod = new GetMethod(socketServiceUrl);
            httpClient.executeMethod(getMethod);
            String response = getMethod.getResponseBodyAsString();
            SocketServiceStatus.INSTANCE.setConnected(true);
            return objectMapper.readValue(response, new TypeReference<List<ProxyChannel>>() {});
        } catch (IOException e) {
            SocketServiceStatus.INSTANCE.setConnected(false);
            throw new ProxyClientException(e);
        }

    }

    @Override
    public int countChannels() throws ProxyClientException {
        if(socketServiceUrl == null){
            throw new ProxyClientException("socket_service_url is not defined");
        }
        try {
            HttpClient httpClient = new HttpClient();
            GetMethod getMethod = new GetMethod(socketServiceUrl+"/count");
            httpClient.executeMethod(getMethod);
            String response = getMethod.getResponseBodyAsString();
            SocketServiceStatus.INSTANCE.setConnected(true);
            return Integer.parseInt(response);
        } catch (IOException e) {
            throw new ProxyClientException(e);
        }
    }

    @Override
    public void stopAllChannels() throws ProxyClientException {
        if(socketServiceUrl == null){
            throw new ProxyClientException("socket_service_url is not defined");
        }
        try {
            HttpClient httpClient = new HttpClient();
            DeleteMethod deleteMethod = new DeleteMethod(socketServiceUrl);
            httpClient.executeMethod(deleteMethod);
            if(deleteMethod.getStatusCode() != 204){
                SocketServiceStatus.INSTANCE.setConnected(false);
                if(deleteMethod.getStatusCode() == 400){
                    throw new ProxyClientException("Error while stopping All channels, at least one channel is not stopped");
                }
                throw new ProxyClientException("Unexpected status code while stopping All channels: "+deleteMethod.getStatusCode());
            }
            SocketServiceStatus.INSTANCE.setConnected(true);
        } catch (IOException e) {
            SocketServiceStatus.INSTANCE.setConnected(false);
            throw new ProxyClientException(e);
        }
    }
}
