package net.ihe.gazelle.proxy.admin.gui;

import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;

/**
 * This class contains the methods to access common applications preferences.
 *
 * @author Cédric Eoche-Duval / KEREVAL Rennes IHE development Project
 * @version 1.0 - 2015, October 23th
 */
public abstract class AbstractApplicationConfigurationManager {

    private static Logger log = LoggerFactory.getLogger(AbstractApplicationConfigurationManager.class);

    private String releaseNoteUrl;
    private String documentation;
    private Boolean worksWithoutCas;
    private String issueTracker;
    private String evsClientUrl;
    private String dicomStorage;
    private String applicationUrl;
    private String proxyOid;
    private Boolean ipLogin;
    private String ipLoginAdmin;
    private Boolean jmsCommunicationEnabled;
    private Boolean adminOnlyMode;
    private String applicationAdminEmail;
    private String applicationAdminName;
    private String applicationAdminTitle;

    protected static String getApplicationProperty(String inPropertyKey) {
        String value = ApplicationConfiguration.getValueOfVariable(inPropertyKey);
        if (value == null) {
            AbstractApplicationConfigurationManager.log.error(
                    "Variable '" + inPropertyKey + "' is missing in 'app_configuration' table.");
        }
        return value;
    }

    @Remove
    @Destroy
    public void destroy() {
        resetApplicationConfiguration();
    }

    public void resetApplicationConfiguration() {
        releaseNoteUrl = null;
        documentation = null;
        worksWithoutCas = null;
        issueTracker = null;
        evsClientUrl = null;
        dicomStorage = null;
        applicationUrl = null;
        proxyOid = null;
        ipLogin = null;
        ipLoginAdmin = null;
        adminOnlyMode = null;
        applicationAdminEmail = null;
        applicationAdminName = null;
        applicationAdminTitle = null;
    }

    public String getReleaseNoteUrl() {
        if (releaseNoteUrl == null) {
            releaseNoteUrl = getApplicationProperty("application_release_notes_url");
        }
        return releaseNoteUrl;
    }

    public String getIssueTracker() {
        if (issueTracker == null) {
            issueTracker = getApplicationProperty("application_issue_tracker");
        }
        return issueTracker;
    }

    public String getDocumentation() {
        if (this.documentation == null) {
            this.documentation = getApplicationProperty("application_documentation");
        }
        if (this.documentation == null) {
            this.documentation = "https://gazelle.ihe.net";
        }
        return this.documentation;
    }

    public boolean isWorksWithoutCas() {
        if (worksWithoutCas == null) {
            String booleanAsString = getApplicationProperty("application_works_without_cas");
            if (booleanAsString == null) {
                this.worksWithoutCas = false;
            } else {
                this.worksWithoutCas = Boolean.valueOf(booleanAsString);
            }
        }
        return worksWithoutCas;
    }

    /**
     * Returns true if the logged in user has role "admin_role" or if the
     * application works without cas
     *
     * @return true if the logged in user has role "admin_role" or if the application works without cas
     */
    public boolean isUserAllowedAsAdmin() {
        GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        return gazelleIdentity.isLoggedIn() && gazelleIdentity.hasRole("admin_role");
    }

    public String getEvsClientUrl() {
        if (evsClientUrl == null) {
            evsClientUrl = getApplicationProperty("evs_client_url");
        }
        return evsClientUrl;
    }

    public String getDicomStorage() {
        if (dicomStorage == null) {
            dicomStorage = getApplicationProperty("storage_dicom");
        }
        return dicomStorage;
    }

    public String getApplicationUrl() {
        if (applicationUrl == null) {
            applicationUrl = getApplicationProperty("application_url");
        }
        return applicationUrl;
    }

    public String getProxyOid() {
        if (proxyOid == null) {
            proxyOid = getApplicationProperty("proxy_oid");
        }
        return proxyOid;
    }

    public Boolean getIpLogin() {
        if (ipLogin == null) {
            String booleanAsString = ApplicationConfiguration.getValueOfVariable("ip_login");
            if (booleanAsString == null) {
                this.ipLogin = false;
            } else {
                this.ipLogin = Boolean.valueOf(booleanAsString);
            }
        }
        return ipLogin;
    }

    public String getIpLoginAdmin() {
        if (this.ipLoginAdmin == null) {
            this.ipLoginAdmin = ApplicationConfiguration.getValueOfVariable("ip_login_admin");
        }
        return ipLoginAdmin;
    }

    public String loginByIP() {
        AbstractApplicationConfigurationManager.log.info("starting authentication without cas");
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }


    public boolean isJmsCommunicationEnabled() {
        if (jmsCommunicationEnabled == null) {
            String booleanAsString = ApplicationConfiguration.getValueOfVariable("jms_communication_is_enabled");
            if (booleanAsString == null) {
                this.jmsCommunicationEnabled = false;
            } else {
                this.jmsCommunicationEnabled = Boolean.valueOf(booleanAsString);
            }
        }
        return jmsCommunicationEnabled;
    }

    public Boolean isAdminOnlyMode() {
        if (adminOnlyMode == null) {
            String booleanAsString = getApplicationProperty("admin_only_mode");
            if (booleanAsString == null) {
                this.adminOnlyMode = false;
            } else {
                this.adminOnlyMode = Boolean.valueOf(booleanAsString);
            }
        }
        return adminOnlyMode;
    }

    public String getApplicationAdminEmail() {
        if (applicationAdminEmail == null) {
            applicationAdminEmail = getApplicationProperty("application_admin_email");
        }
        return applicationAdminEmail;
    }

    public String getApplicationAdminName() {
        if (applicationAdminName == null) {
            applicationAdminName = getApplicationProperty("application_admin_name");
        }
        return applicationAdminName;
    }

    public String getApplicationAdminTitle() {
        if (applicationAdminTitle == null) {
            applicationAdminTitle = getApplicationProperty("application_admin_title");
        }
        return applicationAdminTitle;
    }
}
