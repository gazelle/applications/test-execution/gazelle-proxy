package net.ihe.gazelle.proxy.interlay.dao;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.AbstractMessageQuery;

public class ProxyDAO {

    public static AbstractMessage getMessageByID(Integer id) {
        AbstractMessageQuery abstractMessageQuery = new AbstractMessageQuery();
        abstractMessageQuery.id().eq(id);
        return abstractMessageQuery.getUniqueResult();
    }

    public static AbstractMessage getMessageByIDWithPrivacyKey(Integer id, String privacyKey){

        if (privacyKey != null && !privacyKey.isEmpty()){
            AbstractMessageQuery abstractMessageQuery = new AbstractMessageQuery();
            abstractMessageQuery.id().eq(id);
            abstractMessageQuery.connection().privacyKey().eq(privacyKey);
            return abstractMessageQuery.getUniqueResult();
        }
        return null;
    }

}
