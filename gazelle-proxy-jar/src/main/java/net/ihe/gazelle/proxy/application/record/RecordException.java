package net.ihe.gazelle.proxy.application.record;

public class RecordException extends RuntimeException {

    public RecordException(String message) {
        super(message);
    }

    public RecordException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecordException(Throwable cause) {
        super(cause);
    }
}
