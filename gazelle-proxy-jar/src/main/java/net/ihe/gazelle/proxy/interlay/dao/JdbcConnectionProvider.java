package net.ihe.gazelle.proxy.interlay.dao;

import net.ihe.gazelle.hql.providers.EntityManagerProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;

public class JdbcConnectionProvider {

    private static final JdbcConnectionProvider INSTANCE = new JdbcConnectionProvider();

    private java.sql.Connection jdbcConnection;

    private JdbcConnectionProvider() {
    }

    public java.sql.Connection getConnection() {
        if(jdbcConnection == null){
            try {
                EntityManager entityManager = GenericServiceLoader.getService(EntityManagerProvider.class).provideEntityManager();
                Session session = entityManager.unwrap(Session.class);
                session.doWork(new Work() {
                    @Override
                    public void execute(Connection connection) throws SQLException {
                        jdbcConnection = connection;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jdbcConnection;
    }

    public static synchronized JdbcConnectionProvider getInstance() {
        return INSTANCE;
    }
}
