package net.ihe.gazelle.proxy.managers;

import net.ihe.gazelle.proxy.admin.model.KeyStoreDetails;

import javax.ejb.Local;

@Local
public interface KeyStoreManagerFactory {

    KeyStoreManager getKeyStoreManager(KeyStoreDetails keyStoreDetails);
}
