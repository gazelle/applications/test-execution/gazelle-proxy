package net.ihe.gazelle.proxy.interlay.record;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.application.record.RecordException;
import net.ihe.gazelle.proxy.application.record.RecordService;

import javax.persistence.EntityManager;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class RecordServiceHibernate implements RecordService {

    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();

    @Override
    public String record(final Object content) {
        if(!isEntity(content)){
            throw new RecordException("Object "+content.getClass()+" is not an entity");
        }
        final AtomicInteger atomicReference = new AtomicInteger();
        try{
            EXECUTOR_SERVICE.submit(new Runnable() {
                @Override
                public void run() {
                    try{
                        Integer id = (Integer) HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                            @Override
                            public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                                Object result = entityManager.merge(content);
                                Method getIdMethod = getGetIdMethod(result);
                                return getIdMethod.invoke(result);
                            }
                        });
                        atomicReference.set(id);
                    }
                    catch (HibernateFailure e){
                        throw new RecordException("Error while persisting item: "+content.getClass(), e);
                    }
                }
            }).get();
        }
        catch (InterruptedException | ExecutionException e){
            throw new RecordException("Error while persisting item: "+content.getClass(), e);
        }
        return String.valueOf(atomicReference.get());
    }

    private Method getGetIdMethod(Object content) {
        Method getIdMethod = null;
        try {
            getIdMethod = content.getClass().getMethod("getId");
        } catch (NoSuchMethodException e) {
            throw new RecordException("Object "+content.getClass()+" doesn't provide and id", e);
        }
        return getIdMethod;
    }

    private boolean isEntity(Object content) {
        return content.getClass().isAnnotationPresent(javax.persistence.Entity.class);
    }
}
