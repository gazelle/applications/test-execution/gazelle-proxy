package net.ihe.gazelle.proxy.interlay.dao;


import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfiguration;

import javax.ejb.Local;

@Local
public interface SecuredChannelConfigurationDAO {

    public SecuredChannelConfiguration find();

    public void save(SecuredChannelConfiguration securedChannelConfiguration);
}
