package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.proxy.model.channel.ChannelType;
import net.ihe.gazelle.proxy.model.channel.ProxySide;
import org.jboss.seam.annotations.Name;

import javax.persistence.Entity;
import java.sql.Timestamp;

@Entity
@Name("tlsErrorMessage")
public class TlsErrorMessage extends AbstractMessage {

    private static final long serialVersionUID = 8827116644848725782L;

    public TlsErrorMessage() {
        super();
    }

    public TlsErrorMessage(Timestamp timestamp, String fromIP, Integer localPort, Integer proxyPort, String toIP,
                           Integer remotePort, ProxySide proxySide) {
        super(timestamp, fromIP, localPort, proxyPort, toIP, remotePort, proxySide);
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.TLS_ERROR;
    }

    @Override
    public String getInfoGUI() {
        return "<span style=\"color:red\"><span class=\"gzl-icon-exclamation-triangle\" style=\"color : red\"></span>TLS error</span>";
    }

    @Override
    public String getLabel() {
        return getInfoGUI();
    }
}
