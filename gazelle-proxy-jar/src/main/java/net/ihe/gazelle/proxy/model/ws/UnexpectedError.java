package net.ihe.gazelle.proxy.model.ws;

public class UnexpectedError {

    private String message;

    private UnexpectedError cause;

    public String getMessage() {
        return message;
    }

    public UnexpectedError setMessage(String message) {
        this.message = message;
        return this;
    }

    public UnexpectedError getCause() {
        return cause;
    }

    public UnexpectedError setCause(UnexpectedError cause) {
        this.cause = cause;
        return this;
    }
}
