package net.ihe.gazelle.proxy.managers;

import net.ihe.gazelle.proxy.admin.model.TrustStoreDetails;

import javax.ejb.Local;

@Local
public interface TrustStoreManagerFactory {

    TrustStoreManager getTrustStoreManager(TrustStoreDetails trustStoreDetails);
}
