package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfiguration;
import net.ihe.gazelle.proxy.admin.model.Subject;
import net.ihe.gazelle.proxy.managers.KeyStoreManager;
import net.ihe.gazelle.proxy.managers.SecuredChannelConfigurationManager;
import net.ihe.gazelle.proxy.managers.TrustStoreManager;
import net.ihe.gazelle.proxy.model.tls.keystore.CipherSuiteType;
import net.ihe.gazelle.proxy.model.tls.keystore.KeyStoreLoadingMethod;
import net.ihe.gazelle.proxy.model.tls.keystore.ProtocolType;
import net.ihe.gazelle.proxy.model.tls.trustore.TrustStoreLoadingMethod;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@AutoCreate
@Scope(ScopeType.PAGE)
@Name("securedChannelConfiguration")
@GenerateInterface("SecuredChannelConfigurationLocal")
public class SecuredChannelConfigurationAction implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(SecuredChannelConfigurationAction.class);

    @In
    private SecuredChannelConfigurationManager securedChannelConfigurationManager;

    @In
    private ProxyBean proxyBean;

    private SecuredChannelConfiguration securedChannelConfiguration;
    private KeyStoreManager keyStoreManager;
    private TrustStoreManager trustStoreManager;

    private final List<Subject> subjects = new ArrayList<>();

    @Create
    public void init() {
        this.securedChannelConfiguration = securedChannelConfigurationManager.find();
        this.isValidKeyStore();
        this.isValidTrustStore();
    }


    public boolean isValidTrustStore(){
        try{
            this.trustStoreManager = securedChannelConfigurationManager.getTrustStore(securedChannelConfiguration.getTrustStoreDetails());
            return true;
        }catch (Exception e){
            log.error("Could not init truststore", e);
         return false;
        }
    }

    public boolean isValidKeyStore(){
        try{
            this.keyStoreManager = securedChannelConfigurationManager.getKeyStore(securedChannelConfiguration.getKeyStoreDetails());
            return true;
        }catch (Exception e){
            log.error("Could not init keystore", e);
            return false;
        }
    }

    public boolean isMutualAuthentication() {
        return securedChannelConfiguration.isMutualAuthentication();
    }

    public void setMutualAuthentication(boolean mutualAuthentication) {
        this.securedChannelConfiguration.setMutualAuthentication(mutualAuthentication);
    }

    public List<Subject> getKeyStoreSubjects() {
        return Collections.singletonList(new Subject(keyStoreManager.getProxyCertificate().getSubjectDN()));
    }

    public List<Subject> getTrustStoreSubjects() {
        if(subjects.isEmpty()) {
            X509Certificate[] certificates = trustStoreManager.getAcceptedIssuers();
            for (int i = 0; i < certificates.length; i++) {
                subjects.add(new Subject(certificates[i].getSubjectDN()));
            }
        }
        return subjects;
    }
    public SecuredChannelConfiguration getSecuredChannelConfiguration() {
        return securedChannelConfiguration;
    }

    public void setSecuredChannelConfiguration(SecuredChannelConfiguration securedChannelConfiguration) {
        this.securedChannelConfiguration = securedChannelConfiguration;
    }

    public List<CipherSuiteType> getCipherSuiteTypesEnabled() {
        return this.securedChannelConfiguration.getCipherSuiteTypesEnabled();
    }

    public void setCipherSuiteTypesEnabled(List<CipherSuiteType> cipherSuiteTypesEnabled) {
        this.securedChannelConfiguration.setCipherSuiteTypesEnabled(cipherSuiteTypesEnabled);
    }

    public List<ProtocolType> getProtocolTypesEnabled() {
        return this.securedChannelConfiguration.getProtocolTypesEnabled();
    }

    public void setProtocolTypesEnabled(List<ProtocolType> protocolTypesEnabled) {
        this.securedChannelConfiguration.setProtocolTypesEnabled(protocolTypesEnabled);
    }

    public List<ProtocolType> getAvailableProtocols() {
        return Arrays.asList(ProtocolType.values());
    }

    public List<CipherSuiteType> getAvailableCipherSuites() {
        return Arrays.asList(CipherSuiteType.values());
    }

    public List<TrustStoreLoadingMethod> getAvailableTrustStoreLoadingMethod(){
        return this.trustStoreManager.getAvailableTrustStoreLoadingMethod();
    }

    public TrustStoreLoadingMethod getTrustStoreLoadingMethod(){
        return this.securedChannelConfiguration.getTrustStoreDetails().getLoadingMethod();
    }

    public void setTrustStoreLoadingMethod(TrustStoreLoadingMethod trustStoreLoadingMethod){
        this.securedChannelConfiguration.getTrustStoreDetails().setLoadingMethod(trustStoreLoadingMethod);
    }

    public List<KeyStoreLoadingMethod> getAvailableKeyStoreLoadingMethod(){
        return this.keyStoreManager.getAvailableKeyStoreLoadingMethod();
    }

    public KeyStoreLoadingMethod getKeyStoreLoadingMethod(){
        return this.securedChannelConfiguration.getKeyStoreDetails().getLoadingMethod();
    }

    public void setKeyStoreLoadingMethod(KeyStoreLoadingMethod keyStoreLoadingMethod){
        this.securedChannelConfiguration.getKeyStoreDetails().setLoadingMethod(keyStoreLoadingMethod);
    }

    public List<SelectItem> getAvailableProtocolsAsSelectItems(){
        List<SelectItem> selectItems = new ArrayList<>();
        for (ProtocolType protocolType : ProtocolType.values()) {
            SelectItem selectItem = new SelectItem();
            selectItem.setValue(protocolType);
            selectItem.setLabel(getDisplayLabel(protocolType));
            selectItems.add(selectItem);
        }
        return selectItems;
    }

    public List<SelectItem> getAvailableCipherSuitesAsSelectItems(){
        List<SelectItem> selectItems = new ArrayList<>();
        List<CipherSuiteType> cipherSuiteTypesToDisplay = getCipherSuiteToDisplay();
        for (CipherSuiteType cipherSuiteType : cipherSuiteTypesToDisplay) {
            SelectItem selectItem = new SelectItem();
            selectItem.setValue(cipherSuiteType);
            selectItem.setLabel(getDisplayLabel(cipherSuiteType));
            selectItems.add(selectItem);
        }
        return selectItems;
    }

    private List<CipherSuiteType>  getCipherSuiteToDisplay() {
        List<CipherSuiteType> cipherSuiteTypesToDisplay = new ArrayList<>(Arrays.asList(CipherSuiteType.values()));
        cipherSuiteTypesToDisplay.remove(CipherSuiteType.SSL_NULL_WITH_NULL_NULL);
        cipherSuiteTypesToDisplay.remove(CipherSuiteType.TLS_RSA_WITH_NULL_MD5);
        cipherSuiteTypesToDisplay.remove(CipherSuiteType.TLS_RSA_WITH_NULL_SHA);
        return cipherSuiteTypesToDisplay;
    }

    private String getDisplayLabel(ProtocolType protocolType) {
        StringBuilder sb = new StringBuilder(protocolType.getName());
        if(protocolType.isDeprecated()) {
            sb.append(" (");
            sb.append(ResourceBundle.instance().getString("net.ihe.gazelle.proxy.Deprecated"));
            sb.append(")");
        }
        return sb.toString();
    }

    private String getDisplayLabel(CipherSuiteType cipherSuiteType) {
        StringBuilder sb = new StringBuilder(cipherSuiteType.toString());
        sb.append(" (");
        if(CipherSuiteType.getCipherSuiteRecommendationForTLSv12().contains(cipherSuiteType)){
            sb.append(ResourceBundle.instance().getString("net.ihe.gazelle.proxy.cipherSuites.Recommendation"));
        } else {
            ProtocolType[] protocolTypes = cipherSuiteType.getProtocolTypes();
            for (int i = 0; i < protocolTypes.length; i++) {
                sb.append(protocolTypes[i].getName());
                if(i < protocolTypes.length - 1){
                    sb.append(", ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void save() {
        securedChannelConfigurationManager.save(securedChannelConfiguration);
        proxyBean.restartAllSecuredChannels();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Configuration saved and secured channels restarted");
    }
}
