package net.ihe.gazelle.proxy.dao;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.model.tm.Step;
import net.ihe.gazelle.proxy.model.tm.StepQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class ProxyDAOTM {

    private static final Logger log = LoggerFactory.getLogger(ProxyDAOTM.class);

    public static void updateStepDate(Integer testStepId) {
        try {
            HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                @Override
                public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                    List<Step> steps = getStepsByTMId((Integer) context[0]);
                    for (Step step : steps) {
                        step.setDate(new Date());
                        entityManager.merge(step);
                    }
                    return null;
                        }
            }, testStepId);
        } catch (HibernateFailure hibernateFailure) {
            log.error("updateStepDate : " + hibernateFailure.getMessage());
        } catch (Error e) {
            log.error("updateStepDate : " + e.getMessage());
        }
    }

    public static List<Step> getStepsByTMId(int testStepId) {
        StepQuery stepQuery = new StepQuery();
        stepQuery.tmId().eq(testStepId);
        return stepQuery.getList();
    }

}
