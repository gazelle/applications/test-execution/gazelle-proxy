package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

@Name("channelsLazyView")
@Scope(ScopeType.PAGE)
public class ChannelsLazyView extends LazyDataModel<ProxyChannel> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8037377555209647566L;

    private LazyDataModel<ProxyChannel> lazyModel;

    private ProxyChannel selectedConfig;

    @In
    private ProxyChannelRefList proxyChannelRefList;

    public ProxyChannel getSelectedConfig() {
        return selectedConfig;
    }

    public void setSelectedConfig(ProxyChannel selectedConfig) {
        this.selectedConfig = selectedConfig;
    }

    @PostConstruct
    public void init() {
        List<ProxyChannel> data = proxyChannelRefList.getSrotedProxyChannelsList();
        lazyModel = new LazyProxyListDataModel(data);
    }

    public LazyDataModel<ProxyChannel> getLazyModel() {
        return lazyModel;
    }

    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Proxy configuration selected",
                Integer.toString(((ProxyChannel) event.getObject()).getResponder().getPort()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Proxy configuration Unselected",
                Integer.toString(((ProxyChannel) event.getObject()).getResponder().getPort()));

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    @Override
    public Object getRowKey(ProxyChannel object) {
        return "" + object.getProxyPort();
    }

    @Override
    public ProxyChannel getRowData(String rowKey) {
        List<ProxyChannel> confs = (List<ProxyChannel>) getWrappedData();
        for (ProxyChannel conf : confs) {
            String proxyport = Integer.toString(conf.getProxyPort());
            if (proxyport.equals(rowKey)) {
                return conf;
            }
        }
        return super.getRowData(rowKey);
    }

}
