package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import org.kohsuke.MetaInfServices;

import java.util.Map;

@MetaInfServices(CSPPoliciesPreferences.class)
public class ProxyCSPPoliciesPreferences implements CSPPoliciesPreferences {

    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        return null;
    }

    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return false;
    }

    @Override
    public boolean isContentPolicyActivated() {
        return false;
    }

}
