package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJson;
import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJsonService;
import net.ihe.gazelle.proxy.managers.SecuredChannelConfigurationManager;
import net.ihe.gazelle.proxy.model.ws.ParameterDTO;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import net.ihe.gazelle.proxy.model.ws.tls.TLSConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AutoCreate
@Name("proxyChannelDTOJsonService")
@Scope(ScopeType.APPLICATION)
public class ProxyChannelDTOJsonServiceImpl implements ProxyChannelDTOJsonService {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(ProxyChannelDTOJsonServiceImpl.class);

    @In
    private SecuredChannelConfigurationManager securedChannelConfigurationManager;


    private TLSConfiguration proxyTlsConfiguration;

    @Create
    public void init(){
        proxyTlsConfiguration = new TLSConfigurationService().createTLSConfiguration(securedChannelConfigurationManager);
    }

    @Override
    public ProxyChannel convertToProxyChannel(ProxyChannelDTOJson proxyChannelDTOJson) {
        if(proxyChannelDTOJson.getResponder().getHostname() == null && proxyChannelDTOJson.getResponder().getIp() == null){
            throw new RuntimeException("Hostname or/and IP should be set");
        }
        ProxyChannel proxyChannel = new ProxyChannel()
                .setPersistent(proxyChannelDTOJson.isPersistent())
                .setType(proxyChannelDTOJson.getType())
                .setProxyPort(proxyChannelDTOJson.getProxyPort())
                .setResponder(proxyChannelDTOJson.getResponder())
                .setAdditionalParameters(convertToMap(proxyChannelDTOJson.getAdditionalParameters()))
                ;
        if(proxyChannel.getResponder().getHostname()==null){
            proxyChannel.getResponder().setHostname(proxyChannel.getResponder().getIp());
        } else if (proxyChannel.getResponder().getIp()==null){
            proxyChannel.getResponder().setIp(resolveIpIfHost(proxyChannel.getResponder().getHostname()));
        }
        if(proxyChannelDTOJson.isSecured()){
            proxyChannel.setTlsConfiguration(new TLSConfiguration(proxyTlsConfiguration));
            proxyChannel.getTlsConfiguration().setSniEnabled(proxyChannelDTOJson.isSniEnabled());
        }
        return proxyChannel;
    }

    @Override
    public List<ProxyChannel> convertToProxyChannels(List<ProxyChannelDTOJson> proxyChannelDTOJsons){
        List<ProxyChannel> proxyChannels = new ArrayList<>();
        for (ProxyChannelDTOJson proxyChannelDTOJson : proxyChannelDTOJsons) {
            proxyChannels.add(convertToProxyChannel(proxyChannelDTOJson));
        }
        return proxyChannels;
    }

    public List<ProxyChannelDTOJson> convertToProxyDTOJsons(Iterable<ProxyChannel> proxyChannels){
        List<ProxyChannelDTOJson> proxyChannelDTOJsons = new ArrayList<>();
        for (ProxyChannel proxyChannel : proxyChannels) {
            proxyChannelDTOJsons.add(new ProxyChannelDTOJson(proxyChannel));
        }
        return proxyChannelDTOJsons;
    }

    public String resolveIpIfHost(String host) {
        if(isIp(host)) {
            return host;
        }
        try{
            String resolved = InetAddress.getByName(host).getHostAddress();
            if(resolved == null){
                log.error("Failed to resolve ip, setting host {}", host);
                return host;
            }
            return resolved;
        } catch (Exception e) {
            log.error("Failed to resolve host " + host, e);
            return host;
        }
    }

    private boolean isIp(String host) {
        return host.matches("\\d+\\.\\d+\\.\\d+\\.\\d+");
    }

    private Map<String, String> convertToMap(List<ParameterDTO> additionalParameters) {
        if(additionalParameters == null){
            return null;
        }
        Map<String, String> map = new HashMap<>();
        for (ParameterDTO parameterDTO : additionalParameters) {
            map.put(parameterDTO.getKey(), parameterDTO.getValue());
        }
        return map;
    }



}
